## Sparrow Mahjong Web Client

This is the beginning of our development repo.

### plans

* Primarily using bootstrap for the frontend.
* javascript/jquery for dynamically loading content and issuing requests to the server.

### hands

* formatted using Bootstraps grid
* contains draggable svgs for each tile

### control panel

* buttons for pung/chao/kong/draw/discard which POST to the API.
* share link

### wall

* implemented with an image for each wall state.

### chat window

* simple chat with markdown support.
