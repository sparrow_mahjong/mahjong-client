//a 3d isometric drawn tile
function TileUp(point) {
   //x is very right wall of tile
   //y is top point of that wall
   this.loc = point;
   this.top = this.top();
   this.right = this.right();
   this.left = this.left();
   //TODO: add in images for tile texturing and change fill
}

TileUp.prototype = {
   top: function() {
      var tile_top = new Path();
      tile_top.strokeColor = 'black';

      tile_top.add(
         this.loc, 
         this.loc + new Point(-2*s, -s),
         this.loc + new Point(-3*s, -.5*s),
         this.loc + new Point(-s, .5*s)
      );

      tile_top.closed = true;
      tile_top.fillColor = 'green';

      return tile_top;
   },

   right: function() {
      var tile_right = new Path();
      tile_right.strokeColor = 'black';

      tile_right.add(
         this.loc, 
         this.loc + new Point(0, .5*s),
         this.loc + new Point(-s, s),
         this.loc + new Point(-s, .5*s)
      );

      tile_right.closed = true;
      tile_right.fillColor = 'white';

      return tile_right;
   },

   left: function() {
      var tile_left = new Path();
      tile_left.strokeColor = 'black';

      tile_left.add(
         this.loc + new Point(-s, s), 
         this.loc + new Point(-s, .5*s),
         this.loc + new Point(-3*s, -.5*s),
         this.loc + new Point(-3*s, 0)
      );

      tile_left.closed = true;
      tile_left.fillColor = 'white';

      return tile_left;
   }
};

function TileDown(point) {
   //x is very right wall of tile
   //y is top point of that wall
   this.loc = point;
   this.top = this.top();
   this.left = this.left();
   this.right = this.right();
   //TODO: add in images for tile texturing and change fill
}

TileDown.prototype = {
   top: function() {
      var tile_top = new Path();
      tile_top.strokeColor = 'black';

      tile_top.add(
         this.loc, 
         this.loc + new Point(-2*s, s),
         this.loc + new Point(-3*s, .5*s),
         this.loc + new Point(-s, -.5*s)
      );

      tile_top.closed = true;
      tile_top.fillColor = 'green';

      return tile_top;
   },

   right: function() {
      var tile_right = new Path();
      tile_right.strokeColor = 'black';

      tile_right.add(
         this.loc, 
         this.loc + new Point(0, .5*s),
         this.loc + new Point(-2*s, 1.5*s),
         this.loc + new Point(-2*s, s)
      );

      tile_right.closed = true;
      tile_right.fillColor = 'white';

      return tile_right;
   },

   left: function() {
      var tile_left = new Path();
      tile_left.strokeColor = 'black';

      tile_left.add(
         this.loc + new Point(-2*s, s), 
         this.loc + new Point(-3*s, .5*s),
         this.loc + new Point(-3*s, 1*s),
         this.loc + new Point(-2*s, 1.5*s)
      );

      tile_left.closed = true;
      tile_left.fillColor = 'white';

      return tile_left;
   }
};

function Wall(point) {
   //var this.count = count;
   //var this.start = start;
   this.p = point;
   this.topLeft = this.up(this.p + new Point(20*s, s));
   this.topRight = this.down(this.p + new Point(20*s, s));
   this.bottomLeft = this.down(this.p + new Point(0, 11*s));
   this.bottomRight = this.up(this.p + new Point(40*s, 11*s));
}

Wall.prototype = {
   up: function(point) {
      var wall = [];
      //point is rightmost corner
      for (var i = 0; i < 36; i = i + 1) {
         if(i % 2 === 0) {
            point = point + new Point(0, .5*s);
            wall.push(new TileUp(point));
         } else {
            point = point + new Point(0, -.5*s);
            wall.push(new TileUp(point));
            point = point + new Point(-s, .5*s);
         }
      }

      return wall;
   },

   down: function(point) {
      //point is leftmost corner
      var wall = [];
      point = point + new Point(3*s, -.5*s);
      for (var i = 0; i < 36; i = i + 1) {
         if(i % 2 === 0) {
            point = point + new Point(0, .5*s);
            wall.push(new TileDown(point));
         } else {
            point = point + new Point(0, -.5*s);
            wall.push(new TileDown(point));
            point = point + new Point(s, .5*s);
         }
      }
      return wall;
   }
};

/* ----------------- main --------------- */
var s = 25;
var test = new Wall(new Point(50, 50));
